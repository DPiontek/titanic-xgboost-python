conda create -n xgboost
conda activate xgboost
conda install -y ipykernel pandas numpy scikit-learn py-xgboost graphviz matplotlib python-graphviz statsmodels seaborn
conda install -y -c conda-forge lightgbm catboost altair
