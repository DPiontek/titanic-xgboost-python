import pandas as pd
import numpy as np
import statsmodels.api as sm
import statsmodels.formula.api as smf

import argparse
import os
#import joblib

# Since we get a headerless CSV file we specify the column names here.
feature_columns_names = [
    'Pclass',
    'Name',
    'Sex',
    'Age',
    'Siblings/Spouses Aboard',
    'Parents/Children Aboard',
    'Fare']

label_column = 'Survived'

feature_columns_dtype = {
    'Pclass': np.int64,
    'Name': str,
    'Sex': str,
    'Age': np.float64,
    'Siblings/Spouses Aboard': np.int64,
    'Parents/Children Aboard': np.int64,
    'Fare': np.float64}

label_column_dtype = {'Survived': np.int64}

def merge_two_dicts(x, y):
    z = x.copy()   # start with x's keys and values
    z.update(y)    # modifies z with y's keys and values & returns None
    return z

if __name__ == '__main__':

    parser = argparse.ArgumentParser()

    # Sagemaker specific arguments. Defaults are set in the environment variables.
    parser.add_argument('--output-data-dir', type=str, default=os.environ['SM_OUTPUT_DATA_DIR'])
    parser.add_argument('--model-dir', type=str, default=os.environ['SM_MODEL_DIR'])
    parser.add_argument('--train', type=str, default=os.environ['SM_CHANNEL_TRAIN'])

    args = parser.parse_args()

    print(args)
    
    # Take the set of files and read them all into a single pandas dataframe
    input_files = [ os.path.join(args.train, file) for file in os.listdir(args.train) ]
    if len(input_files) == 0:
        raise ValueError(('There are no files in {}.\n' +
                          'This usually indicates that the channel ({}) was incorrectly specified,\n' +
                          'the data specification in S3 was incorrectly specified or the role specified\n' +
                          'does not have permission to access the data.').format(args.train, "train"))
    
    raw_data = [ pd.read_csv(
        file, 
#        header=None, 
#        names=feature_columns_names + [label_column],
        dtype=merge_two_dicts(feature_columns_dtype, label_column_dtype)) for file in input_files ]
    concat_data = pd.concat(raw_data)
    
    # Fit logistic regression


    FORM = 'Survived ~ C(Pclass) + C(Sex) + Age + C(Pclass):C(Sex) + C(Pclass):Age + C(Sex):Age'
    model = smf.glm(formula=FORM, data=concat_data, family=sm.families.Binomial(), missing="drop")
    result = model.fit()

    #joblib.dump(result, os.path.join(args.model_dir, "model.joblib"))

    # Create artificial data and predict over the parameterspace
    import itertools

    def expand_grid(**data_dict):
        rows = itertools.product(*data_dict.values())
        return pd.DataFrame.from_records(rows, columns=data_dict.keys())

    # Generate a grid of the parameter space for predictions and plotting
    # Survived is not used, but allows to use FORM on artificial data 
    ndat = expand_grid(Survived=[1], Sex=['male','female'], Age=range(0,71), Pclass=[1,2,3])

    ypred = result.get_prediction(ndat, transform=True).summary_frame()

    ndm = pd.concat([ndat,ypred], axis=1)
    ndm['Pclass'] = ndm['Pclass'].map({1:'First Class', 2:'Second Class', 3:'Third Class'})
    ndm.rename(columns={'mean':'P(Survival)'}, inplace=True)

    # Plot Survivalrates over the parameter space
    import seaborn as sns
    sns.set()
    sns.set_style("whitegrid")

    import matplotlib.pyplot as plt

    #g = sns.FacetGrid(data=ndm, col="Pclass", hue="Sex", sharey=True, aspect=1, height=6, legend_out=True)
    g = sns.FacetGrid(data=ndm, col="Pclass", hue="Sex", sharey=True, aspect=1, legend_out=True)
    g.map(plt.fill_between, 'Age', 'mean_ci_lower', 'mean_ci_upper', alpha=0.2);
    g.map(plt.plot, 'Age', 'P(Survival)', alpha=1.0).add_legend();
    #g.fig.suptitle( f"Survival rate on the Titanic\n     Logistic regression model equation: {FORM} \n     95% confidence intervals, N:" + str(data.shape[0])
    #    ,position=(0,1.15), fontweight='bold', size=18, horizontalalignment='left')
    print(f"Survival rate on the Titanic\n     Logistic regression model equation: {FORM}"
          f" \n     95% confidence intervals, N:" + str(concat_data.shape[0]))
    g.savefig(os.path.join(args.output_data_dir,"titanic.png"))

    
def input_fn(input_data, content_type):
    """Parse input data payload
    
    We currently only take csv input. Since we need to process both labelled
    and unlabelled data we first determine whether the label column is present
    by looking at how many columns were provided.
    """
    if content_type == 'text/csv':
        # Read the raw input data as CSV.
        df = pd.read_csv(StringIO(input_data))
        return df
    else:
        raise ValueError("{} not supported by script!".format(content_type))
        

def output_fn(prediction, accept):
    """Format prediction output
    
    The default accept/content-type between containers for serial inference is JSON.
    We also want to set the ContentType or mimetype as the same value as accept so the next
    container can read the response payload correctly.
    """
    if accept == 'text/csv':
        return worker.Response(encoders.encode(prediction, accept), mimetype=accept)
    else:
        raise RuntimeException("{} accept type is not supported by this script.".format(accept))


def predict_fn(input_data, model):
    """Preprocess input data
    
    We implement this because the default predict_fn uses .predict(), but our model is a preprocessor
    so we want to use .transform().

    The output is returned in the following order:
    
        rest of features either one hot encoded or standardized
    """
    features = model.result.get_prediction(input_data, transform=True).summary_frame()
    return features
    

def model_fn(model_dir):
    """Deserialize fitted model
    """
    #model = joblib.load(os.path.join(model_dir, "model.joblib"))
    return model